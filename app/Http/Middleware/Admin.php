<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || auth()->user()->rol != 'admin'){
            return redirect()
                ->route('home')
                ->with('msg', 'Acceso denegado');
        }

        return $next($request);
    }
}
