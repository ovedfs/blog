<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(App\User::class)->create([
			'name' => 'Oved Fiso',
			'email' => 'ovedfs@gmail.com',
			'rol' => 'admin',
			'password' => bcrypt('123456')
		]);
	}
}
