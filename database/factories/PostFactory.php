<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
	return [
		'title' => $title = $faker->sentence(8),
		'slug' => str_slug($title),
		'body' => $faker->text(100),
		'image' => $faker->imageUrl(640, 480),
		'user_id' => 1
	];
});
