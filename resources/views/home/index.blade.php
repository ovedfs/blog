@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="card-columns">
			@foreach($posts as $post)
				@include('home.card')
			@endforeach
		</div>
	</div>
@endsection
