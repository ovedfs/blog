<div class="card bg-dark text-white">
	<img class="card-img-top" src="{{ $post->image }}" alt="Card image cap">

	<div class="card-body">
		<h5 class="card-title">{{ $post->title }}</h5>
		<p class="card-text">{{ $post->body }}</p>
	</div>
	
	<div class="card-footer text-center">
		<a href="#" class="btn btn-primary">Leer mas</a>
	</div>
</div>