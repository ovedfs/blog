@extends('layouts.app')

@section('content')
	<div class="container">
		<h1>ADMIN BLOG</h1><hr>

		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Imagen</th>
					<th>Título</th>
					<th>Contenido</th>
					<th>Creador</th>
					<th>Creación</th>
					<th>Actualización</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($posts as $post)
					<tr>
						<td>{{ $loop->index + 1 }}</td>
						<td>
							<img src="{{ $post->image }}" width="100">
						</td>
						<td>{{ $post->title }}</td>
						<td>{{ $post->body }}</td>
						<td>{{ $post->user->name }}</td>
						<td>{{ $post->created_at->diffForHumans() }}</td>
						<td>{{ $post->updated_at->diffForHumans() }}</td>
						<td>Editar | Eliminar</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection
